﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//guns objects in 'Player's' hierarchy
[System.Serializable]
public class Guns
{
    public GameObject centralGun;
    [HideInInspector] public ParticleSystem centralGunVFX;
}

public class PlayerShooting : MonoBehaviour
{
    [Tooltip("shooting frequency. the higher the more frequent")]
    public float fireRate;

    [Tooltip("projectile prefab")] public GameObject projectileObject;

    [SerializeField] private AudioClip _playerShootingAudioClip;
    private AudioSource _playerShootingAudioSource;

    //time for a new shot
    [HideInInspector] public float nextFire;

    public Guns guns;
    bool shootingIsActive = true;
    public static PlayerShooting instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        _playerShootingAudioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        guns.centralGunVFX = guns.centralGun.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (shootingIsActive)
        {
            if (Time.time > nextFire)
            {
                MakeAShot();
                nextFire = Time.time + 1 / fireRate;
            }
        }
    }

    //method for a shot
    void MakeAShot()
    {
        _playerShootingAudioSource.PlayOneShot(_playerShootingAudioClip);
        CreateLazerShot(projectileObject, guns.centralGun.transform.position, Vector3.zero);
        guns.centralGunVFX.Play();
    }

    void CreateLazerShot(GameObject lazer, Vector3 pos,
        Vector3 rot) //translating 'pooled' lazer shot to the defined position in the defined rotation
    {
        Instantiate(lazer, pos, Quaternion.Euler(rot));
    }
}