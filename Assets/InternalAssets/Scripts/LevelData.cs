﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "DataStore/LevelData")]
public class LevelData : ScriptableObject
{
    [SerializeField] private int levelNumber;
    [SerializeField] private int hazardCount;
    [SerializeField] private int howMuchScorePoints;
    [SerializeField] private Sprite backgroundTop;
    [SerializeField] private Sprite backgroundBottom;


    public int LevelNumber => levelNumber;
    public int HazardCount => hazardCount;
    public int HowMuchScorePoints => howMuchScorePoints;
    public Sprite BackgroundTop => backgroundTop;
    public Sprite BackgroundBottom => backgroundBottom;
}