﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsEnemy : MonoBehaviour
{
    #region FIELDS

    [Tooltip("Health points in integer")] public int health;


    [Tooltip("VFX prefab generating after destruction")]
    public GameObject destructionVFX;

    #endregion

    private void Start()
    {
    }


    //method of getting damage for the 'Enemy'
    public void GetDamage(int damage)
    {
        health -= damage; //reducing health for damage value, if health is less than 0, starting destruction procedure
        if (health <= 0)
            Destruction();
    }

    //if 'Enemy' collides 'Player', 'Player' gets the damage equal to projectile's damage value
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TriggerEvent("PlayerHasBeenDamaged");
            Destruction();
        }
    }

    //method of destroying the 'Enemy'
    void Destruction()
    {
        Instantiate(destructionVFX, transform.position, Quaternion.identity);

        TriggerEvent("HazardDestroyed");
        Destroy(gameObject);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}