﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileMoving : MonoBehaviour
{
    [Tooltip("Moving speed on Y axis in local space")]
    public float speed;

    private float _heightCam;
    private Vector2 _centrCam; // центр камеры (это ее пивот)

    private static float _minY; // левая и правая границы

    private void Start()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _heightCam = main.orthographicSize;

            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        _minY = _centrCam.y - _heightCam;
    }

    //moving the object with the defined speed
    private void Update()
    {
        if (transform.position.y > _minY)
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        else
        {
            Destroy(gameObject);
        }
    }
}