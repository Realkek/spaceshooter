﻿using UnityEngine;
using System.Collections;

public class EvasiveMoving : MonoBehaviour
{
    public float tilt;
    public float dodge;
    public float smoothing;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;

    private float currentSpeed;
    private float targetManeuver;

    private Rigidbody2D m_rigidbody;

    private float _xMin = -6f;
    private float _xMax = 6f;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();

        // currentSpeed = GetComponent<Rigidbody>().velocity.z;
        StartCoroutine(Evade());
    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
        while (true)
        {
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }

    void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(m_rigidbody.velocity.x, targetManeuver, smoothing * Time.deltaTime);
        m_rigidbody.velocity = new Vector2(newManeuver, 0.0f);
        m_rigidbody.position = new Vector2
        (
            Mathf.Clamp(m_rigidbody.position.x, _xMin, _xMax),
            0.0f
        );
    }
}