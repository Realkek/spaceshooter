﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject GameOverMenuPanel;
    [SerializeField] private GameObject MenuManager;
    private Animator GameOverPanelAnimator;

    private void Start()
    {
        GameOverPanelAnimator = GameOverMenuPanel.GetComponent<Animator>();
        Subscribe();
    }

    public void ShowGameOverPanel()
    {
        MenuManager.GetComponent<PanelManager>().OpenPanel(GameOverPanelAnimator);
    }

    private void HideGameOverPanel()
    {
        gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GameOver", ShowGameOverPanel);
        ManagerEvents.StartListening("LevelCompleted", HideGameOverPanel);
    }


    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}