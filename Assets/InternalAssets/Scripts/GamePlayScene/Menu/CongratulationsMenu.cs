﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CongratulationsMenu : MonoBehaviour
{
    [SerializeField] private GameObject CongratulationsMenuPanel;
    [SerializeField] private GameObject MenuManager;
    private Animator CongratulationsPanelAnimator;

    private void Start()
    {
        CongratulationsPanelAnimator = CongratulationsMenuPanel.GetComponent<Animator>();
        Subscribe();
    }

    public void CongratulationsPanel()
    {
        MenuManager.GetComponent<PanelManager>().OpenPanel(CongratulationsPanelAnimator);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("LevelCompleted", CongratulationsPanel);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}