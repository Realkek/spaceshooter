﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextLevelButton : MonoBehaviour, IEventSub
{
    private string _currentLevel;

    [SerializeField] private GameObject nextLevelButtonLabel;

    private void Start()
    {
        Subscribe();
        _currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
    }

    public void LoadNextlevel()
    {
        switch (_currentLevel)
        {
            case "Level1":
                PlayerPrefs.SetString("currentLevelLoaded", "Level2");
                SceneManager.LoadScene("GamePlay");
                break;
            case "Level2":
                PlayerPrefs.SetString("currentLevelLoaded", "Level3");
                SceneManager.LoadScene("GamePlay");
                break;
            case "Level3":
                PlayerPrefs.SetString("currentLevelLoaded", "Level3");
                SceneManager.LoadScene("GamePlay");
                break;
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("LevelCompleted", CheckCurrentLevelLoaded);
    }

    private void CheckCurrentLevelLoaded()
    {
        _currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
        if (_currentLevel == "Level3")
            nextLevelButtonLabel.GetComponent<Text>().text = "Retry";
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}