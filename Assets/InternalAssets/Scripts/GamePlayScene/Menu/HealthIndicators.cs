﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIndicators : MonoBehaviour, IEventSub
{
    // Start is called before the first frame update
    void Start()
    {
        Subscribe();
    }


    private void HideHealthIndicators()
    {
        gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("LevelCompleted", HideHealthIndicators);
    }


    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}