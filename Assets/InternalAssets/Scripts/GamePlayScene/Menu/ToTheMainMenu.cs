﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToTheMainMenu : MonoBehaviour
{
    public void GoToTheMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}