﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rigid2DMover : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;

   

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _rigidbody2D.velocity = new Vector3(0, -6, 0);
    }
}