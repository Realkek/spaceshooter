﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidMover : MonoBehaviour
{
    public float speed;
    private Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        _rigidbody.velocity = new Vector3(0, -6, 0);
    }
}