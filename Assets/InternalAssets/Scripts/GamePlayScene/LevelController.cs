﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour, IEventSub, IEventTrigger
{
    [SerializeField] private List<LevelData> _levelData;

    [SerializeField] private GameObject healthIndicators;
    private int healthCount;

    public AudioSource audioSource;
    public AudioClip winSound;
    public AudioClip looseSound;

    // Prefabs
    public GameObject[] hazards;

    // Game Parameters
    public Vector3 spawnValues;
    public int hazardCount;
    private int _nextLevelNumber;
    public float spawnWait;
    public float waveWait;
    string _currentLevel;
    [SerializeField] private GameObject scorePanel;
    [SerializeField] private GameObject backgroundThemeTop;
    [SerializeField] private GameObject backgroundThemeBottom;
    private float _widthCam; // ширина камеры
    private Vector2 _centrCam; // центр камеры (это ее пивот)

    protected static float MinX, MaxX; // левая и правая границы

    // States
    private enum eGameState
    {
        GAME_STATE_PLAYING,
    }

    private eGameState m_state = eGameState.GAME_STATE_PLAYING;

    private enum ePlayState
    {
        PLAY_STATE_STARTUP,
        PLAY_STATE_WAVE,
        PLAY_STATE_IN_BETWEEN_WAVES
    }

    private ePlayState m_playState = ePlayState.PLAY_STATE_STARTUP;


    // other game vars
    private int m_score = 0;
    private int m_hazardSpawned = 0;

    // calculated on the fly
    private double m_accuracy = 0;

    // Timers
    private float m_startupTime;
    private float m_spawnTime;
    private float m_gameOverTime;
    private float m_scoreTime;

    private void Awake()
    {
        InitializationLevelData();
        CheckSaveState();
    }

    void Start()
    {
        Subscribe();
        if (Camera.main != null)
        {
            var main = Camera.main;
            _widthCam = main.orthographicSize *
                        main.aspect; // Получаем половину ширины камеры, путем умножения высоты на соотношение
            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        MinX = _centrCam.x - _widthCam; // левый край  (отнимаем от центра половину ширины)
        MaxX = _centrCam.x + _widthCam; // правый край (прибавляем к центру половину ширины)
    }

    private void InitializationLevelData()
    {
        _currentLevel = PlayerPrefs.GetString("currentLevelLoaded");
        switch (_currentLevel)
        {
            case "Level1":
                hazardCount = _levelData[0].HazardCount;
                scorePanel.GetComponent<TextMeshProUGUI>().text = _levelData[0].HowMuchScorePoints.ToString();
                _nextLevelNumber = _levelData[0].LevelNumber + 1;
                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = _levelData[0].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = _levelData[0].BackgroundBottom;
                break;
            case "Level2":
                hazardCount = _levelData[1].HazardCount;
                scorePanel.GetComponent<TextMeshProUGUI>().text = _levelData[1].HowMuchScorePoints.ToString();
                _nextLevelNumber = _levelData[0].LevelNumber + 2;
                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = _levelData[1].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = _levelData[1].BackgroundBottom;
                break;
            case "Level3":
                hazardCount = _levelData[2].HazardCount;
                scorePanel.GetComponent<TextMeshProUGUI>().text = _levelData[2].HowMuchScorePoints.ToString();
                _nextLevelNumber = _levelData[0].LevelNumber + 1;

                backgroundThemeTop.GetComponent<SpriteRenderer>().sprite = _levelData[2].BackgroundTop;
                backgroundThemeBottom.GetComponent<SpriteRenderer>().sprite = _levelData[2].BackgroundBottom;
                break;
        }

        TriggerEvent("LevelInitialized");
    }

    void Update()
    {
        Quit();
        switch (m_state)
        {
            case eGameState.GAME_STATE_PLAYING:
                switch (m_playState)
                {
                    case ePlayState.PLAY_STATE_STARTUP:
                    case ePlayState.PLAY_STATE_IN_BETWEEN_WAVES:
                        m_startupTime -= Time.deltaTime;
                        if (m_startupTime <= 0)
                        {
                            StartWave();
                        }

                        break;
                    case ePlayState.PLAY_STATE_WAVE:
                        m_spawnTime -= Time.deltaTime;
                        if (m_spawnTime <= 0)
                        {
                            if (m_hazardSpawned >= hazardCount)
                            {
                                // We have done the wave.
                                m_playState = ePlayState.PLAY_STATE_IN_BETWEEN_WAVES;
                                m_startupTime = waveWait;
                            }
                            else
                            {
                                m_hazardSpawned++;
                                m_spawnTime = spawnWait;
                                SpawnHazard();
                            }
                        }

                        break;
                }

                break;
        }
    }

    private void SetHealthCount()
    {
        switch (healthCount)
        {
            case 1:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 2:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 3:
                healthIndicators.transform.GetChild(0).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(1).gameObject.SetActive(true);
                healthIndicators.transform.GetChild(2).gameObject.SetActive(true);
                break;
        }

        {
        }
    }

    private void CheckSaveState()
    {
        if (PlayerPrefs.GetInt("IsGameSaved") == 1)
        {
            scorePanel.GetComponent<TextMeshProUGUI>().text =
                PlayerPrefs.GetString("SavedScore");
            healthCount = PlayerPrefs.GetInt("SavedHealth");
            SetHealthCount();
            PlayerPrefs.SetInt("IsGameSaved", 0);
        }
        else
        {
            healthCount = 3;
            SetHealthCount();
        }
    }

    private void Quit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerPrefs.SetString("SavedScore", scorePanel.GetComponent<TextMeshProUGUI>().text);
            CheckHealthCount();
            PlayerPrefs.SetInt("SavedHealth", healthCount);
            PlayerPrefs.SetInt("IsGameSaved", 1);
            SceneManager.LoadScene("MainMenu");
        }
    }

    void CheckHealthCount()
    {
        healthCount = 0;
        for (int healthCounter = 0; healthCounter < healthIndicators.transform.childCount; healthCounter++)
        {
            if (healthIndicators.transform.GetChild(healthCounter).gameObject.activeSelf)
                healthCount++;
        }
    }


    void StartWave()
    {
        m_hazardSpawned = 1;
        m_spawnTime = spawnWait;
        m_playState = ePlayState.PLAY_STATE_WAVE;
        SpawnHazard();
    }


    void SpawnHazard()
    {
        GameObject hazard = hazards[UnityEngine.Random.Range(0, hazards.Length)];
        Vector3 spawnPosition = new Vector3(UnityEngine.Random.Range(MinX + 0.5f, MaxX - 0.5f), spawnValues.y,
            spawnValues.z);
        Quaternion spawnRotation = Quaternion.identity;
        Instantiate(hazard, spawnPosition, spawnRotation);
    }


    private void DisableLevelController()
    {
        PlayerPrefs.SetInt("IsGameSaved", 0);
        gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GameOver", DisableLevelController);
        ManagerEvents.StartListening("GameOver", PlayLooseSound);
        ManagerEvents.StartListening("LevelCompleted", OpenNewLevel);
        ManagerEvents.StartListening("LevelCompleted", PlayWinSound);
        ManagerEvents.StartListening("LevelCompleted", DisableLevelController);
    }

    private void PlayLooseSound()
    {
        audioSource.PlayOneShot(looseSound);
    }

    private void PlayWinSound()
    {
        audioSource.PlayOneShot(winSound);
    }

    private void OpenNewLevel()
    {
        PlayerPrefs.SetInt($"LevelCompleted{_currentLevel}", 1);
        PlayerPrefs.SetInt($"LevelOpened{_nextLevelNumber}", 1);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}