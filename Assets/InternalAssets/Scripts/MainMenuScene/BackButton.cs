﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : MonoBehaviour
{
    [SerializeField] private GameObject levelMenuPanel;
    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject MenuManager;
    private Animator BackButtonAnimator;

    private void Start()
    {
        BackButtonAnimator = mainMenuPanel.GetComponent<Animator>();
    }

    public void ShowMainMenuPanel()
    {
        MenuManager.GetComponent<PanelManager>().OpenPanel(BackButtonAnimator);
        levelMenuPanel.SetActive(false);
    }
}