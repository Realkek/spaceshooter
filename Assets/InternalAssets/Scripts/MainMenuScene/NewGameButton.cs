﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameButton : MonoBehaviour
{
    [SerializeField] private GameObject levelMenuPanel;
    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject MenuManager;
    private Animator NewGameMenuButtonAnimator;

    private void Start()
    {
        NewGameMenuButtonAnimator = levelMenuPanel.GetComponent<Animator>();
    }

    public void ShowLevelMenuPanel()
    {
        MenuManager.GetComponent<PanelManager>().OpenPanel(NewGameMenuButtonAnimator);
        mainMenuPanel.SetActive(false);
    }
}