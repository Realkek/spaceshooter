﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelLoader : MonoBehaviour
{
    public abstract void LoadLevel();
}