﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayLoader : MonoBehaviour
{
    public void LoadGamePlay()
    {
        if (PlayerPrefs.GetInt("IsGameSaved") == 1)
            SceneManager.LoadScene("GamePlay");
    }
}