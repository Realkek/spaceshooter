﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButton2 : LevelLoader
{
    [SerializeField] private GameObject lockLevel;
    [SerializeField] private GameObject tickLevel;

    private void Start()
    {
        lockLevel.SetActive(true);
        tickLevel.SetActive(false);
        if (PlayerPrefs.GetInt("LevelOpened2") == 1)
        {
            lockLevel.SetActive(false);
            tickLevel.SetActive(false);
        }

        if (PlayerPrefs.GetInt("LevelCompletedLevel2") == 1)
        {
            lockLevel.SetActive(false);
            tickLevel.SetActive(true);
        }
    }

    public override void LoadLevel()
    {
        if (PlayerPrefs.GetInt("LevelOpened2") == 1)
        {
            PlayerPrefs.SetInt("IsGameSaved", 0);
            PlayerPrefs.SetString("currentLevelLoaded", "Level2");
            SceneManager.LoadScene("GamePlay");
        }
    }
}