﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueLabel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("IsGameSaved") == 1)
            GetComponent<Text>().color = Color.green;
        else if (PlayerPrefs.GetInt("IsGameSaved") == 0)
        {
            GetComponent<Text>().color = Color.red;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}