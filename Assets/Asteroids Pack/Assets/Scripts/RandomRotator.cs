﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour
{
    [SerializeField] private float tumble;

    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-50, 50) * tumble;
    }
}